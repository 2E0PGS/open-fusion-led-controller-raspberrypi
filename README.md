# Open Fusion LED Controller Project

## This is the Raspberry Pi code for the project

API and webpage server.

### Links to other repositories

* Main repository for documentation and diagrams: [open-fusion-led-controller-main](https://bitbucket.org/2E0PGS/open-fusion-led-controller-main)
* Core Arduino repository: [open-fusion-led-controller-arduino](https://bitbucket.org/2E0PGS/open-fusion-led-controller-arduino)
* Web Arduino repository: [open-fusion-led-controller-web-arduino](https://bitbucket.org/2E0PGS/open-fusion-led-controller-web-arduino)
* Raspberry Pi repository: [open-fusion-led-controller-raspberrypi](https://bitbucket.org/2E0PGS/open-fusion-led-controller-raspberrypi)
* Windows remote repository: [open-fusion-led-controller-win-remote](https://bitbucket.org/2E0PGS/open-fusion-led-controller-win-remote)

## Crontab examples

Turn lights on and off automatically.

```
# Turn OFLC light on in morning.
30 7 * * * /usr/bin/curl http://192.168.1.111:5000/?setpower=1 > /dev/null 2>&1

# Turn OFLC light off when I must leave for work.
35 8 * * * /usr/bin/curl http://192.168.1.111:5000/?setpower=0 > /dev/null 2>&1
```

Start Python API on boot.

```
@reboot screen -S oflc -d -m /usr/bin/python3 /home/pi/open-fusion-led-controller-raspberrypi/oflc.py
```

## compile-and-upload.sh 

This is if you want to upload new firmware to the Arduino from the Raspberry Pi. I have some flags for the old bootloader that may not be applicable to your AVR.

```
#!/bin/bash

./arduino-cli compile ~/open-fusion-led-controller-arduino --fqbn arduino:avr:nano:cpu=atmega328old

./arduino-cli upload ~/open-fusion-led-controller-arduino --port /dev/ttyUSB0 --fqbn arduino:avr:nano:cpu=atmega328old 
```
