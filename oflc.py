# ##################################################
# Open Fusion LED Controller Project.
# Intended for use with Raspberry Pi.
# Main Repository: https://bitbucket.org/2E0PGS/open-fusion-led-controller-main
# Raspberry Pi Repository: https://bitbucket.org/2E0PGS/open-fusion-led-controller-raspberrypi
# ##################################################

# Copyright (C) <2017-2019>  <Peter Stevenson> (2E0PGS)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, render_template, request
import serial
import urllib.parse

ser = serial.Serial('/dev/ttyUSB0')

app = Flask(__name__)

@app.route('/')
def index():
    power = request.args.get('setpower')
    output = request.args.get('setoutput')
    mode = request.args.get('setmode')
    timer = request.args.get('settimer')
    debug = request.args.get('setdebug')
    brightness = request.args.get('setbrightness')
    custom = request.args.get('setcustom')
    #numpixels = request.args.get('setnumpixels')

    # Specific to web page only.
    web_custom_hex = request.args.get('websetcustomhex')
    web_custom_hex_button = request.args.get('websetcustomhexbutton')
    web_timer = request.args.get('websettimer')
    web_timer_button = request.args.get('websettimerbutton')
    web_brightness = request.args.get('websetbrightness')
    web_brightness_button = request.args.get('websetbrightnessbutton')

    if power:
        ser.write(str.encode('setpower, ' + power + '\n'))
    if output:
        ser.write(str.encode('setoutput, ' + output + '\n'))
    if mode:
        ser.write(str.encode('setmode, ' + mode + '\n'))
    if timer:
        ser.write(str.encode('settimer, ' + timer + '\n'))
    if debug:
        ser.write(str.encode('setdebug, ' + debug + '\n'))
    if brightness:
        ser.write(str.encode('setbrightness, ' + brightness + '\n'))
    if custom:
        custom_unquoted = urllib.parse.unquote(custom)
        ser.write(str.encode('setcustom, ' + custom_unquoted + '\n'))
    #if numpixels:
        #ser.write(str.encode('setnumpixels, ' + numpixels + '\n'))
    
    # Specific to web page only.
    if web_custom_hex and web_custom_hex_button:
        web_custom_hex_unquoted = urllib.parse.unquote(web_custom_hex)
        customhex_to_rgb = web_custom_hex_unquoted.lstrip('#')
        customhex_to_rgb_trup = tuple(int(customhex_to_rgb[i:i+2], 16) for i in (0, 2 ,4))
        customhex_to_rgb_str = ', '.join(map(str, customhex_to_rgb_trup))
        ser.write(str.encode('setcustom, ' + customhex_to_rgb_str + '\n'))
    if web_timer and web_timer_button:
        ser.write(str.encode('settimer, ' + web_timer + '\n'))
    if web_brightness and web_brightness_button:
        ser.write(str.encode('setbrightness, ' + web_brightness + '\n'))

    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
